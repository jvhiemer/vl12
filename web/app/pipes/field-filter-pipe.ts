
import { Pipe, PipeTransform } from 'angular2/core';

@Pipe({
  name: 'fieldFilter'
})
export class FieldFilterPipe implements PipeTransform {
  transform(value: string, args: any[]) {
    var result = [];
    var fields = args[0];
    if (!value)
      return result;

    Object.keys(value).forEach(function(key, $index) {
      if (fields.indexOf(key) !== -1)
        return;

      var obj = fields[key];
      obj.key = key;

      result.push(obj);
    });
    return result;
  }
}
