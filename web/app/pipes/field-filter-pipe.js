System.register(['angular2/core'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var FieldFilterPipe;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            FieldFilterPipe = (function () {
                function FieldFilterPipe() {
                }
                FieldFilterPipe.prototype.transform = function (value, args) {
                    var result = [];
                    var fields = args[0];
                    if (!value)
                        return result;
                    Object.keys(value).forEach(function (key, $index) {
                        if (fields.indexOf(key) !== -1)
                            return;
                        var obj = fields[key];
                        obj.key = key;
                        result.push(obj);
                    });
                    return result;
                };
                FieldFilterPipe = __decorate([
                    core_1.Pipe({
                        name: 'fieldFilter'
                    }), 
                    __metadata('design:paramtypes', [])
                ], FieldFilterPipe);
                return FieldFilterPipe;
            })();
            exports_1("FieldFilterPipe", FieldFilterPipe);
        }
    }
});
//# sourceMappingURL=field-filter-pipe.js.map