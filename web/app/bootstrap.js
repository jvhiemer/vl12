System.register(['angular2/platform/browser', 'angular2/core', 'angular2/router', 'angular2/http', './services/http/http-transformer', './services/data/data-service', './services/authentication/authentication-service', './services/authentication/storage-service', './components/app'], function(exports_1) {
    var browser_1, core_1, router_1, http_1, http_transformer_1, data_service_1, authentication_service_1, storage_service_1, app_1;
    var globalContextInjections;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (http_transformer_1_1) {
                http_transformer_1 = http_transformer_1_1;
            },
            function (data_service_1_1) {
                data_service_1 = data_service_1_1;
            },
            function (authentication_service_1_1) {
                authentication_service_1 = authentication_service_1_1;
            },
            function (storage_service_1_1) {
                storage_service_1 = storage_service_1_1;
            },
            function (app_1_1) {
                app_1 = app_1_1;
            }],
        execute: function() {
            globalContextInjections = [
                router_1.ROUTER_PROVIDERS,
                http_1.HTTP_PROVIDERS,
                http_transformer_1.HttpTransformer,
                data_service_1.DataService,
                authentication_service_1.AuthenticationService,
                storage_service_1.StorageService,
                core_1.bind(router_1.LocationStrategy).toClass(router_1.HashLocationStrategy)
            ];
            browser_1.bootstrap(app_1.App, [globalContextInjections]);
        }
    }
});
//# sourceMappingURL=bootstrap.js.map