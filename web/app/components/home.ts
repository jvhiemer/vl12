
import { Component, View } from 'angular2/core';
import { RouterLink } from 'angular2/router';

@Component({
	selector: 'home'
})
@View({
	directives: [RouterLink],
	templateUrl: 'app/partials/home.html'
})
export class Home {

}
