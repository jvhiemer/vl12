System.register(['angular2/core', 'angular2/router', '../components/home', '../components/profile/login-form', '../components/profile/register-form', 'ng2-bootstrap/ng2-bootstrap'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, home_1, login_form_1, register_form_1, ng2_bootstrap_1;
    var App;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (home_1_1) {
                home_1 = home_1_1;
            },
            function (login_form_1_1) {
                login_form_1 = login_form_1_1;
            },
            function (register_form_1_1) {
                register_form_1 = register_form_1_1;
            },
            function (ng2_bootstrap_1_1) {
                ng2_bootstrap_1 = ng2_bootstrap_1_1;
            }],
        execute: function() {
            App = (function () {
                function App() {
                }
                App = __decorate([
                    core_1.Component({
                        selector: 'app'
                    }),
                    core_1.View({
                        directives: [
                            ng2_bootstrap_1.DROPDOWN_DIRECTIVES,
                            router_1.RouterLink, router_1.RouterOutlet,
                            login_form_1.LoginForm, register_form_1.RegisterForm
                        ],
                        templateUrl: 'app/partials/menu/menu.html'
                    }),
                    router_1.RouteConfig([
                        { path: '/', component: home_1.Home, as: 'Home' },
                        { path: '/login', component: login_form_1.LoginForm, as: 'Login' },
                        { path: '/register', component: register_form_1.RegisterForm, as: 'Register' }
                    ]), 
                    __metadata('design:paramtypes', [])
                ], App);
                return App;
            })();
            exports_1("App", App);
        }
    }
});
//# sourceMappingURL=app.js.map