
import { Component, View } from 'angular2/core';
import { RouteConfig, RouterLink, RouterOutlet } from 'angular2/router';

import { Home } from '../components/home';

import { LoginForm } from '../components/profile/login-form';
import { RegisterForm } from '../components/profile/register-form';

import { DROPDOWN_DIRECTIVES } from 'ng2-bootstrap/ng2-bootstrap';

@Component({
	selector: 'app'
})

@View({
	directives: [
		DROPDOWN_DIRECTIVES,
		RouterLink, RouterOutlet,
		LoginForm, RegisterForm
	],
	templateUrl: 'app/partials/menu/menu.html'
})

@RouteConfig([
  { path: '/', component: Home, as: 'Home'},
	{ path: '/login', component: LoginForm, as: 'Login' },
	{ path: '/register', component: RegisterForm, as: 'Register' }
])

export class App {}
