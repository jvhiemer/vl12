
import { Component, View, Inject } from 'angular2/core';
import { FORM_DIRECTIVES } from 'angular2/common';
import { RouterLink, RouteParams } from 'angular2/router';

import { Register } from './register';

@Component({
	selector: 'register-form'
})
@View({
	directives: [FORM_DIRECTIVES, RouterLink],
	templateUrl: 'app/partials/profile/register.html'
})

export class RegisterForm {
	model = new Register('','','','','');
}
