System.register(['angular2/core', 'angular2/common', 'angular2/router', '../../services/authentication/authentication-service', './login'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1, router_1, authentication_service_1, login_1;
    var LoginForm;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (authentication_service_1_1) {
                authentication_service_1 = authentication_service_1_1;
            },
            function (login_1_1) {
                login_1 = login_1_1;
            }],
        execute: function() {
            LoginForm = (function () {
                function LoginForm(authenticationService, _router) {
                    this.authenticationService = authenticationService;
                    this._router = _router;
                    this.model = new login_1.Login('', '');
                    this.powers = ['Really Smart', 'Super Flexible',
                        'Super Hot', 'Weather Changer'];
                }
                LoginForm.prototype.onSubmit = function () {
                    this.authenticationService.login(this.model);
                    this._router.navigate(['MonitorDashboard']);
                };
                Object.defineProperty(LoginForm.prototype, "diagnostic", {
                    get: function () { return JSON.stringify(this.model); },
                    enumerable: true,
                    configurable: true
                });
                LoginForm = __decorate([
                    core_1.Component({
                        selector: 'login-form',
                        templateUrl: 'app/partials/profile/login.html',
                        directives: [router_1.RouterLink, common_1.NgForm, common_1.NgModel]
                    }), 
                    __metadata('design:paramtypes', [authentication_service_1.AuthenticationService, router_1.Router])
                ], LoginForm);
                return LoginForm;
            })();
            exports_1("LoginForm", LoginForm);
        }
    }
});
//# sourceMappingURL=login-form.js.map