
import { Component } from 'angular2/core';
import { NgForm, NgModel } from 'angular2/common';
import { RouterLink, Router, RouteParams } from 'angular2/router';
import { AuthenticationService } from '../../services/authentication/authentication-service';

import { Login } from './login';

@Component({
	selector: 'login-form',
	templateUrl: 'app/partials/profile/login.html',
	directives: [RouterLink, NgForm, NgModel]
})
export class LoginForm {
	model = new Login('','');

	powers = ['Really Smart', 'Super Flexible',
					 'Super Hot', 'Weather Changer'];

	constructor(private authenticationService: AuthenticationService,
		private _router:Router) {}

	onSubmit() {
		this.authenticationService.login(this.model);
		this._router.navigate(['MonitorDashboard'])
	}

	get diagnostic() { return JSON.stringify(this.model); }
}
