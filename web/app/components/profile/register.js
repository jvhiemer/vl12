System.register([], function(exports_1) {
    var Register;
    return {
        setters:[],
        execute: function() {
            Register = (function () {
                function Register(username, firstName, lastName, password, confirmPassword) {
                    this.username = username;
                    this.firstName = firstName;
                    this.lastName = lastName;
                    this.password = password;
                    this.confirmPassword = confirmPassword;
                }
                return Register;
            })();
            exports_1("Register", Register);
        }
    }
});
//# sourceMappingURL=register.js.map