
import { bootstrap } from 'angular2/platform/browser';
import { bind } from 'angular2/core';

import { ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy } from 'angular2/router';
import { HTTP_PROVIDERS } from 'angular2/http';

import { HttpTransformer } from './services/http/http-transformer';
import { DataService } from './services/data/data-service';

import { AuthenticationService } from './services/authentication/authentication-service';
import { StorageService } from './services/authentication/storage-service';

import { App } from './components/app';

var globalContextInjections = [
  ROUTER_PROVIDERS,
  HTTP_PROVIDERS,
  HttpTransformer,
  DataService,

  AuthenticationService,
  StorageService,
  
  bind(LocationStrategy).toClass(HashLocationStrategy)
];

bootstrap(App, [globalContextInjections]);
