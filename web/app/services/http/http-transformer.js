System.register(['angular2/core', 'angular2/http', 'rxjs/add/operator/do', '../authentication/storage-service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, storage_service_1;
    var HttpTransformer;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {},
            function (storage_service_1_1) {
                storage_service_1 = storage_service_1_1;
            }],
        execute: function() {
            HttpTransformer = (function () {
                function HttpTransformer(http, storageService) {
                    this.http = null;
                    this.storageService = null;
                    this.http = http;
                    this.storageService = storageService;
                }
                HttpTransformer.prototype.request = function (url, options) {
                    var _this = this;
                    var req;
                    if (typeof url === 'string') {
                        var reqOpt = new http_1.RequestOptions(options);
                        reqOpt.url = url;
                        req = new http_1.Request(reqOpt);
                    }
                    else {
                        req = url;
                    }
                    this._beforeCall(req);
                    return this.http.request(req)
                        .do(function (res) { _this._afterCall(req, res); });
                };
                HttpTransformer.prototype.get = function (url, options) {
                    var opts = this._build(http_1.RequestMethod.Get, url, options);
                    return this.request(url, opts);
                };
                HttpTransformer.prototype.post = function (url, body, options) {
                    var opts = this._build(http_1.RequestMethod.Post, url, options, body);
                    return this.request(url, opts);
                };
                HttpTransformer.prototype.put = function (url, body, options) {
                    var opts = this._build(http_1.RequestMethod.Put, url, options, body);
                    return this.request(url, opts);
                };
                HttpTransformer.prototype.patch = function (url, body, options) {
                    var opts = this._build(http_1.RequestMethod.Patch, url, options, body);
                    return this.request(url, opts);
                };
                HttpTransformer.prototype.delete = function (url, options) {
                    var opts = this._build(http_1.RequestMethod.Delete, url, options);
                    return this.request(url, opts);
                };
                HttpTransformer.prototype._getRequestPath = function (req) {
                    return req.url;
                };
                HttpTransformer.prototype._beforeCall = function (req) {
                    var path = this._getRequestPath(req);
                    var content = path;
                    if (req.method !== http_1.RequestMethod.Get && req.method !== http_1.RequestMethod.Head) {
                        content += req.text();
                    }
                    var authHeader = this.storageService.load();
                    if (path.indexOf('88.198.249.61') == -1)
                        req.headers.set('X-Auth-Token', authHeader);
                    req.headers.set('Content-Type', 'application/json;charset=utf-8');
                    req.headers.set('Accept', 'application/json;charset=utf-8');
                };
                HttpTransformer.prototype._afterCall = function (req, res) {
                    var etag = res.headers.get('x-etag');
                    var token = res.headers.get('X-Auth-Token');
                    if (token)
                        this.storageService.storeToken(token);
                    var path = this._getRequestPath(req);
                };
                HttpTransformer.prototype._build = function (method, url, options, body) {
                    var aBody = body ? body : options && options.body ? options.body : undefined;
                    var opts = {
                        method: method,
                        url: url,
                        headers: options && options.headers ? options.headers : new http_1.Headers(),
                        search: options && options.search ? options.search : undefined,
                        body: aBody
                    };
                    return opts;
                };
                HttpTransformer = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http, storage_service_1.StorageService])
                ], HttpTransformer);
                return HttpTransformer;
            })();
            exports_1("HttpTransformer", HttpTransformer);
        }
    }
});
//# sourceMappingURL=http-transformer.js.map