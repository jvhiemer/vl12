
import { Injectable, Inject, forwardRef} from 'angular2/core';

import { Http, Request, Headers, Response, RequestOptionsArgs, RequestOptions, RequestMethod } from 'angular2/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do'

import { StorageService } from '../authentication/storage-service';

@Injectable()
export class HttpTransformer {
  http = null;
  storageService = null;

  constructor(http: Http, storageService: StorageService) {
    this.http = http;
    this.storageService = storageService;
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    var req: Request;
    if (typeof url === 'string') {
      var reqOpt = new RequestOptions(options);
      reqOpt.url = url;
      req = new Request(reqOpt);
    } else {
      req = url;
    }

    this._beforeCall(req);

    return this.http.request(req)
      .do((res:Response) => { this._afterCall(req, res) });
  }
  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    var opts: RequestOptionsArgs = this._build(RequestMethod.Get, url, options);
    return this.request(url, opts);
  }
  post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    var opts: RequestOptionsArgs = this._build(RequestMethod.Post, url, options, body);
    return this.request(url, opts);
  }
  put(url: string,  body: string, options?: RequestOptionsArgs): Observable<Response> {
    var opts: RequestOptionsArgs = this._build(RequestMethod.Put, url, options, body);
    return this.request(url, opts);
  }
  patch(url: string,  body: string, options?: RequestOptionsArgs): Observable<Response> {
    var opts: RequestOptionsArgs = this._build(RequestMethod.Patch, url, options, body);
    return this.request(url, opts);
  }
  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    var opts: RequestOptionsArgs = this._build(RequestMethod.Delete, url, options);
    return this.request(url, opts);
  }

  private _getRequestPath(req: Request): string {
    return req.url;
  }

  private _beforeCall(req: Request): void {
    var path = this._getRequestPath(req);

    var content = path;
    if (req.method !== RequestMethod.Get && req.method !== RequestMethod.Head) {
      content += req.text();
    }
    
    var authHeader = this.storageService.load();
    if (path.indexOf('88.198.249.61') == -1)
      req.headers.set('X-Auth-Token', authHeader);
    req.headers.set('Content-Type', 'application/json;charset=utf-8');
    req.headers.set('Accept', 'application/json;charset=utf-8');
  }

  private _afterCall(req: Request, res: Response) {
    var etag: string = res.headers.get('x-etag');
    var token: string = res.headers.get('X-Auth-Token');

    if (token)
      this.storageService.storeToken(token);

    var path = this._getRequestPath(req);
  }

  private _build(method: RequestMethod, url: string, options: RequestOptionsArgs, body?: string): RequestOptionsArgs {
    var aBody = body ? body : options && options.body ? options.body : undefined
    var opts: RequestOptionsArgs = {
      method: method,
      url: url,
      headers: options && options.headers ? options.headers : new Headers(),
      search: options && options.search ? options.search : undefined,
      body: aBody
    };
    return opts;
  }
}
