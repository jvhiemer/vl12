
import { Component, Injectable } from 'angular2/core';
import { Observable, Subject, ReplaySubject } from 'rxjs';

import { HttpTransformer } from '../http/http-transformer';
import { Http } from 'angular2/http';
import { StorageService } from './storage-service'

@Injectable()
export class AuthenticationService {

  constructor(private http: Http, private httpTransformer: HttpTransformer,
    private storageService: StorageService) { }

	logError(err) {
		console.log(err)
	}

  public login(user) {
    this.httpTransformer.post('http://localhost:8080/login', JSON.stringify(user))
    .map(res => res.json())
    .subscribe(
      data => this.storageService.store(data),
      err => this.logError(err),
      () => console.log("We have completed the request dude")
    );
  }
}
