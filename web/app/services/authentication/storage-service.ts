
import { Component, Injectable } from 'angular2/core';

@Injectable()
export class StorageService {

  store(user) {
    localStorage.setItem('app.user.id', user.id);
    localStorage.setItem('app.user.token', user.token);
  }

  load() : string {
    return localStorage.getItem('app.user.token');
  }

  storeToken(token: string) {
    localStorage.setItem('app.user.token', token);
  }
}
