
import { Injectable } from 'angular2/core';

import { HttpTransformer } from '../http/http-transformer'

import { Response } from 'angular2/http';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

@Injectable()
export class DataService {
  baseUrl = 'http://localhost:8080'

  constructor(private http: HttpTransformer) {
  }

  list(url) : any {
    return this.http.get(this.baseUrl + url)
      .map(
        (res: Response) => res.json()
      )
      .map(
        (res: any) => {
          return res.content;
        }
      );
  }

}
